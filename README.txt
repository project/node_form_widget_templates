Description
-----------
This is a very simple theme add-in
It does not add any functionality that isn't already available
It does add template files for each of the widget types (elements) of forms that can be reused as outlined in https://api.drupal.org/api/drupal/modules!field!field.module/function/theme_field/7
Thus making forms very easy to theme.

Use
---
The default templates are found in the templates folder of this module
1) Copy the template that you need to your theme directory
2) This template will automatically be found
3) Alter the template to meet your requirements
4) If a template needs to be specific then it can be made specific to a content type, or even a field within that content type using suggestion
5) Suggestion uses the format webform_templates_field__-<field name>__<content type>.tpl.php
